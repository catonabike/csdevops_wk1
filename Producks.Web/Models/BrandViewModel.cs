﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Producks.Web.Models
{
    public class BrandViewModel
    {
        [Required]
        [Display(Name= "Brand Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name="Brand Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name="Brand is Active?")]
        public bool Active { get; set; }


        public static IEnumerable<BrandViewModel> brandList(IEnumerable<Data.Brand> theBrands)
        {
            var bvm = theBrands.Select(b => new Models.BrandViewModel()
            {
                Id = b.Id,
                Name = b.Name,
                Active = b.Active
            }).AsEnumerable();

            return bvm;
        }

        public static BrandViewModel brandDetail(Data.Brand theBrand)
        {
            var bvm = new BrandViewModel
            {
                Id = theBrand.Id,
                Name = theBrand.Name,
                Active = theBrand.Active
            };

            return bvm;
        }

        public static Data.Brand brandBuild(BrandViewModel theBrand)
        {
            var bvm = new Data.Brand();

            bvm.Id = theBrand.Id;
            bvm.Name = theBrand.Name;
            bvm.Active = theBrand.Active;

            return bvm;
        }
    }
}

