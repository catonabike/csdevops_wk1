﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Producks.Web.Models
{
    public class CategoryViewModel
    {
        [Required]
        [Display(Name = "Category Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Category Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Category is Active?")]
        public bool Active { get; set; }


        public static IEnumerable<CategoryViewModel> categoryList(IEnumerable<Data.Category> theCategories)
        {
            var cvm = theCategories.Select(b => new Models.CategoryViewModel()
            {
                Id = b.Id,
                Name = b.Name,
                Description = b.Description,
                Active = b.Active
            }).AsEnumerable();

            return cvm;
        }

        public static CategoryViewModel categoryDetail(Data.Category theCategory)
        {
            var cvm = new CategoryViewModel
            {
                Id = theCategory.Id,
                Name = theCategory.Name,
                Description = theCategory.Description,
                Active = theCategory.Active
            };

            return cvm;
        }

        public static Data.Category categoryBuild(CategoryViewModel theCategory)
        {
            var cvm = new Data.Category();

            cvm.Id = theCategory.Id;
            cvm.Name = theCategory.Name;
            cvm.Description = theCategory.Description;
            cvm.Active = theCategory.Active;

            return cvm;
        }
    }
}
