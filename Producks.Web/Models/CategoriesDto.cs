﻿using System;

namespace Producks.Web.Models
{
    public class CategoriesDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
