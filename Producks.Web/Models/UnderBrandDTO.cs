﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Producks.Data;

namespace Producks.Web.Models
{
    public class UnderBrandDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AvailableProductCount { get; set; }

        public static IEnumerable<UnderBrandDTO> toBrandList(IEnumerable<Data.Brand> theBrands)
        {
            var buc = theBrands.Select(b => new Models.UnderBrandDTO()
            {
                Id = b.Id,
                Name = b.Name
            }).AsEnumerable();

            return buc;
        }

        public static IEnumerable<UnderBrandDTO> acticeBrands(IEnumerable<Product> theProducts)
        {
            var bvm = theProducts
                .Where(b => b.Active == true)
                .Select(b => new UnderBrandDTO()
            {
                Id = b.Brand.Id,
                Name = b.Brand.Name
            })
            .AsEnumerable()
            .GroupBy(x => new { x.Name })
            .Select(y => y.First());

            return bvm;
        }
    }
}
