﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Producks.Data;

namespace Producks.Web.Models
{
    public class UnderCategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AvailableProductCount { get; set; }

        public static IEnumerable<UnderCategoryDTO> toCategoryList(IEnumerable<Data.Category> theCategories)
        {
            var cuc = theCategories.Select(b => new Models.UnderCategoryDTO()
            {
                Id = b.Id,
                Name = b.Name
            }).AsEnumerable();

            return cuc;
        }


        public static IEnumerable<UnderCategoryDTO> activeCategories(IEnumerable<Product> theProducts)
        {
            var cvm = theProducts
                .Where(c => c.Active == true)
                .Select(c => new UnderCategoryDTO()
            {
                Id = c.Category.Id,
                Name = c.Category.Name,
                Description = c.Category.Description
            })
            .AsEnumerable()
            .GroupBy(x => new { x.Name })
            .Select(y => y.First());

            return cvm;
        }
    }
}
