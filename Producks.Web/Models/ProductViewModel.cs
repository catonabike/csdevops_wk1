﻿using Producks.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Producks.Web.Models
{
    public class ProductViewModel
    {
        [Required]
        [Display(Name = "Product Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Category Id")]
        public int CategoryId { get; set; }

        [Required]
        [Display(Name = "Brand Id")]
        public int BrandId { get; set; }

        [Required]
        [Display(Name = "Product Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [Required]
        [Display(Name = "Stock Available")]
        public int StockLevel { get; set; }

        [Required]
        [Display(Name = "Product is Active?")]
        public bool Active { get; set; }

        [Display(Name = "Category Name")]
        public Category Category { get; set; }

        [Display(Name = "Brand Name")]
        public Brand Brand { get; set; }


        public static IEnumerable<ProductViewModel> productList(IEnumerable<Product> theProducts)
        {
            var pvm = theProducts.Select(b => new ProductViewModel()
            {
                Id = b.Id,
                Name = b.Name,
                Description = b.Description,
                Price = b.Price,
                StockLevel = b.StockLevel,
                Active = b.Active,
                Category = b.Category,
                Brand = b.Brand
            }).AsEnumerable();

            return pvm;
        }

        public static ProductViewModel productDetail(Product theProduct)
        {
            var pvm = new ProductViewModel
            {
                Id = theProduct.Id,
                CategoryId = theProduct.CategoryId,
                BrandId = theProduct.BrandId,
                Name = theProduct.Name,
                Description = theProduct.Description,
                Price = theProduct.Price,
                StockLevel = theProduct.StockLevel,
                Active = theProduct.Active,
                Category = theProduct.Category,
                Brand = theProduct.Brand
            };

            return pvm;
        }

        public static Product productBuild(ProductViewModel theProduct)
        {
            var pvm = new Product();

            pvm.Id = theProduct.Id;
            pvm.CategoryId = theProduct.CategoryId;
            pvm.BrandId = theProduct.BrandId;
            pvm.Name = theProduct.Name;
            pvm.Description = theProduct.Description;
            pvm.Price = theProduct.Price;
            pvm.StockLevel = theProduct.StockLevel;
            pvm.Active = theProduct.Active;
            pvm.Category = theProduct.Category;
            pvm.Brand = theProduct.Brand;

            return pvm;
        }

        public static List<BrandViewModel> chooseBrand(IEnumerable<Product> theProducts)
        {
            var bvm = theProducts.Select(b => new BrandViewModel()
            {
                Id = b.Brand.Id,
                Name = b.Brand.Name,
                Active = b.Brand.Active
            })
            .AsEnumerable()
            .GroupBy(x => new { x.Name })
            .Select(y => y.First());

            return bvm.ToList();
        }

        public static List<CategoryViewModel> chooseCategory(IEnumerable<Product> theProducts)
        {
            var cvm = theProducts.Select(c => new CategoryViewModel()
            {
                Id = c.Category.Id,
                Name = c.Category.Name,
                Description = c.Category.Description,
                Active = c.Category.Active
            })
            .AsEnumerable()
            .GroupBy(x => new { x.Name })
            .Select(y => y.First());

            return cvm.ToList();
        }

        public static List<BrandViewModel> showActiveBrand(IEnumerable<Product> theProducts)
        {
            var bvm = theProducts
                .Where(b => b.Brand.Active == true)
                .Select(b => new BrandViewModel()
            {
                Id = b.Brand.Id,
                Name = b.Brand.Name,
                Active = b.Brand.Active
            })
            .AsEnumerable()
            .GroupBy(x => new { x.Name })
            .Select(y => y.First());

            return bvm.ToList();
        }

        public static List<CategoryViewModel> showActiveCategory(IEnumerable<Product> theProducts)
        {
            var cvm = theProducts
                .Where(b => b.Category.Active == true)
                .Select(c => new CategoryViewModel()
            {
                Id = c.Category.Id,
                Name = c.Category.Name,
                Description = c.Category.Description,
                Active = c.Category.Active
            })
            .AsEnumerable()
            .GroupBy(x => new { x.Name })
            .Select(y => y.First());

            return cvm.ToList();
        }

        public static List<ProductViewModel> showActiveProduct(IEnumerable<Product> theProducts, int? categoryId, int? brandId, int? minPrice, int? maxPrice)
        {
            var pvm = theProducts
                .Where(b => (!categoryId.HasValue && b.Category.Active) || (categoryId.HasValue && b.CategoryId == categoryId && b.Category.Active))
                .Where(b => (!brandId.HasValue && b.Brand.Active) || (brandId.HasValue && b.BrandId == brandId && b.Brand.Active))
                .Where(b => !minPrice.HasValue || (minPrice.HasValue && b.Price >= minPrice))
                .Where(b => !maxPrice.HasValue || (maxPrice.HasValue && b.Price <= maxPrice))
                .Where(b => b.Active)
                .Select(b => new ProductViewModel()
            {
                Id = b.Id,
                CategoryId = b.CategoryId,
                BrandId = b.BrandId,
                Name = b.Name,
                Description = b.Description,
                Price = b.Price,
                StockLevel = b.StockLevel,
                Active = b.Active
            }).ToList();

            return pvm;
        }
    }
}

