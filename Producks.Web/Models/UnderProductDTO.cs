﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Producks.Data;

namespace Producks.Web.Models
{
    public class UnderProductDTO
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Ean { get; set; }
        public double Price { get; set; }
        public bool InStock { get; set; }
        public string ExpectedRestock { get; set; }


        public static IEnumerable<UnderProductDTO> toProductList(IEnumerable<Product> theList, string cFilter, string bFilter)
        {
            if (cFilter == "All" && bFilter == "All")
            {
                var pm = theList
                    .Where(b => b.Active == true)
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.Category.Id,
                        CategoryName = b.Category.Name,
                        BrandId = b.Brand.Id,
                        BrandName = b.Brand.Name,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = "Unavailable",
                        Price = b.Price,
                        InStock = true,
                        ExpectedRestock = "Unknown"
                    }).AsEnumerable();

                return pm;
            }
            else if (cFilter == "All")
            {
                var pm = theList
                    .Where(b => b.Active == true)
                    .Where(b => b.Brand.Name == bFilter)
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.Category.Id,
                        CategoryName = b.Category.Name,
                        BrandId = b.Brand.Id,
                        BrandName = b.Brand.Name,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = "Unavailable",
                        Price = b.Price,
                        InStock = true,
                        ExpectedRestock = "Unknown"
                    }).AsEnumerable();

                return pm;
            }
            else if (bFilter == "All")
            {
                var pm = theList
                    .Where(b => b.Active == true)
                    .Where(b => b.Category.Name == cFilter)
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.Category.Id,
                        CategoryName = b.Category.Name,
                        BrandId = b.Brand.Id,
                        BrandName = b.Brand.Name,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = "Unavailable",
                        Price = b.Price,
                        InStock = true,
                        ExpectedRestock = "Unknown"
                    }).AsEnumerable();

                return pm;
            }
            else
            {
                var pm = theList
                    .Where(b => b.Active == true)
                    .Where(b => b.Category.Name == cFilter)
                    .Where(b => b.Brand.Name == bFilter)
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.Category.Id,
                        CategoryName = b.Category.Name,
                        BrandId = b.Brand.Id,
                        BrandName = b.Brand.Name,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = "Unavailable",
                        Price = b.Price,
                        InStock = true,
                        ExpectedRestock = "Unknown"
                    }).AsEnumerable();

                return pm;
            }
        }

        public static IEnumerable<UnderProductDTO> filterUcProducts(IEnumerable<UnderProductDTO> theList, string cFilter, string bFilter)
        {
            if (cFilter == "All" && bFilter == "All")
            {
                var pm = theList
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.CategoryId,
                        CategoryName = b.CategoryName,
                        BrandId = b.BrandId,
                        BrandName = b.BrandName,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = b.Ean,
                        Price = b.Price,
                        InStock = b.InStock,
                        ExpectedRestock = b.ExpectedRestock
                    }).AsEnumerable();
                    
                    return pm;
            }
            else if (cFilter == "All")
            {
                var pm = theList
                    .Where(b => b.BrandName == bFilter)
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.CategoryId,
                        CategoryName = b.CategoryName,
                        BrandId = b.BrandId,
                        BrandName = b.BrandName,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = b.Ean,
                        Price = b.Price,
                        InStock = b.InStock,
                        ExpectedRestock = b.ExpectedRestock
                    }).AsEnumerable();
                    
                    return pm;
            }
            else if (bFilter == "All")
            {
                var pm = theList
                    .Where(b => b.CategoryName == cFilter)
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.CategoryId,
                        CategoryName = b.CategoryName,
                        BrandId = b.BrandId,
                        BrandName = b.BrandName,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = b.Ean,
                        Price = b.Price,
                        InStock = b.InStock,
                        ExpectedRestock = b.ExpectedRestock
                    }).AsEnumerable();
                    
                    return pm;
            }
            else
            {
                var pm = theList
                    .Where(b => b.CategoryName == cFilter)
                    .Where(b => b.BrandName == bFilter)
                    .Select(b => new UnderProductDTO()
                    {
                        Id = b.Id,
                        CategoryId = b.CategoryId,
                        CategoryName = b.CategoryName,
                        BrandId = b.BrandId,
                        BrandName = b.BrandName,
                        Name = b.Name,
                        Description = b.Description,
                        Ean = b.Ean,
                        Price = b.Price,
                        InStock = b.InStock,
                        ExpectedRestock = b.ExpectedRestock
                    }).AsEnumerable();

                    return pm;
            }
        }
    }
}
