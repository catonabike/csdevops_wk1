﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Producks.Web.Tools
{
    public class UnderProductsDtoCompare : IEqualityComparer<Models.UnderProductDTO>
    {
        public bool Equals(Models.UnderProductDTO x, Models.UnderProductDTO y)
        {
            return x.Name == y.Name;
        }

        public int GetHashCode(Models.UnderProductDTO obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
