﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Producks.Web.Tools
{
    public class UnderBrandsDtoCompare : IEqualityComparer<Models.UnderBrandDTO>
    {
        public bool Equals(Models.UnderBrandDTO x, Models.UnderBrandDTO y)
        {
            return x.Name == y.Name;
        }

        public int GetHashCode(Models.UnderBrandDTO obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
