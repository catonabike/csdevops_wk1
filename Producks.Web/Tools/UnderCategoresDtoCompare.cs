﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Producks.Web.Tools
{
    public class UnderCategoresDtoCompare : IEqualityComparer<Models.UnderCategoryDTO>
    {
        public bool Equals(Models.UnderCategoryDTO x, Models.UnderCategoryDTO y)
        {
            return x.Name == y.Name;
        }

        public int GetHashCode(Models.UnderCategoryDTO obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
