﻿using Producks.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Producks.Web.External
{
    public class Undercutters
    {
        HttpClient client;
        
        public Undercutters()
        {
            client = new HttpClient();
            client.BaseAddress = new System.Uri("http://undercutters.azurewebsites.net/");
            client.DefaultRequestHeaders.Accept.ParseAdd("apllication/json");
        }

        public IEnumerable<UnderCategoryDTO> UcCategories { get; set; }
        public bool GetUcCategoriesError { get; set; }
        public IEnumerable<UnderBrandDTO> UcBrands { get; set; }
        public bool GetUcBrandsError { get; set; }
        public IEnumerable<UnderProductDTO> UcProducts { get; set; }
        public bool GetUcProductsError { get; set; }
        

        // List of categories from undercutters.
        public async Task getUcCategories()
        {
            HttpResponseMessage response = await client.GetAsync("api/Category");

            if (response.IsSuccessStatusCode)
            {
                UcCategories = await response.Content
                    .ReadAsAsync<IEnumerable<UnderCategoryDTO>>();
            }
            else
            {
                GetUcCategoriesError = true;
                UcCategories = Array.Empty<UnderCategoryDTO>();
            }
        }

        // List of brands from undercutters.
        public async Task getUcBrands()
        {
            HttpResponseMessage response = await client.GetAsync("api/Brand");

            if (response.IsSuccessStatusCode)
            {
                UcBrands = await response.Content
                    .ReadAsAsync<IEnumerable<UnderBrandDTO>>();
            }
            else
            {
                GetUcBrandsError = true;
                UcBrands = Array.Empty<UnderBrandDTO>();
            }
        }

        // We will just get the full list & then filter locally if required. Then, if seperate calls for category prods and brand prods we will not get 2 calls!
        public async Task getUcProducts()
        {
            HttpResponseMessage response = await client.GetAsync("api/Product");

            if (response.IsSuccessStatusCode)
            {
                UcProducts = await response.Content
                    .ReadAsAsync<IEnumerable<UnderProductDTO>>();
            }
            else
            {
                GetUcBrandsError = true;
                UcProducts = Array.Empty<UnderProductDTO>();
            }
        }
    }
}
