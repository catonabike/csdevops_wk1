using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Web.Repository;

namespace Producks.Web.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IGenericRepository<Product> _productRepo;

        public ProductsController(IGenericRepository<Product> productRepo)
        {
            _productRepo = productRepo;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            var allProducts = await _productRepo.Get(
                includeProperties: "Category,Brand");

            var viewModel = Models.ProductViewModel.productList(allProducts);

            return View(viewModel);
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();

                var product = await _productRepo.GetByFilters(
                    filter: p => p.Id == id,
                    includeProperties: "Category,Brand");

                if (product == null)
                {
                    return NotFound();
                }

                var viewModel = Models.ProductViewModel.productDetail(product);

                return View(viewModel);
            }
        }

        // GET: Products/Create
        public async Task<IActionResult> Create()
        {
            var product = await _productRepo.Get(
                includeProperties: "Category,Brand");

            var bl = Models.ProductViewModel.chooseBrand(product);
            var cl = Models.ProductViewModel.chooseCategory(product);

            ViewData["BrandId"] = new SelectList(bl, "Id", "Name");
            ViewData["CategoryId"] = new SelectList(cl, "Id", "Name");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CategoryId,BrandId,Name,Description,Price,StockLevel,Active")] Models.ProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                var model = Models.ProductViewModel.productBuild(product);

                await _productRepo.Insert(model);
                await _productRepo.Save();

                return RedirectToAction(nameof(Index));
            }

            var pList = await _productRepo.Get(
                includeProperties: "Category,Brand");

            var bl = Models.ProductViewModel.chooseBrand(pList);
            var cl = Models.ProductViewModel.chooseCategory(pList);

            ViewData["BrandId"] = new SelectList(bl, "Id", "Name", product.BrandId);
            ViewData["CategoryId"] = new SelectList(cl, "Id", "Name", product.CategoryId);
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();

                var product = await _productRepo.GetById(okId);
                var pList = await _productRepo.Get(
                    includeProperties: "Category,Brand");

                if (product == null)
                {
                    return NotFound();
                }
                else
                {
                    var viewModel = Models.ProductViewModel.productDetail(product);
                    var bl = Models.ProductViewModel.chooseBrand(pList);
                    var cl = Models.ProductViewModel.chooseCategory(pList);

                    ViewData["BrandId"] = new SelectList(bl, "Id", "Name", viewModel.BrandId);
                    ViewData["CategoryId"] = new SelectList(cl, "Id", "Name", viewModel.CategoryId);
                    return View(viewModel);
                }
            }
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CategoryId,BrandId,Name,Description,Price,StockLevel,Active")] Models.ProductViewModel product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var model = Models.ProductViewModel.productBuild(product);

                    _productRepo.Update(model);
                    await _productRepo.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    bool exist = await ProductExists(product.Id);

                    if (!exist)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var pList = await _productRepo.Get(
                    includeProperties: "Category,Brand");

            var bl = Models.ProductViewModel.chooseBrand(pList);
            var cl = Models.ProductViewModel.chooseCategory(pList);

            ViewData["BrandId"] = new SelectList(bl, "Id", "Name", product.BrandId);
            ViewData["CategoryId"] = new SelectList(cl, "Id", "Name", product.CategoryId);
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();

                var product = await _productRepo.GetByFilters(
                    filter: p => p.Id == id,
                    includeProperties: "Category,Brand");

                if (product == null)
                {
                    return NotFound();
                }

                var viewModel = Models.ProductViewModel.productDetail(product);

                return View(viewModel);
            }
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _productRepo.Delete(id);
            await _productRepo.Save();

            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ProductExists(int id)
        {
            return await _productRepo.Exists(id);
        }
    }
}
