using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Web.Repository;

namespace Producks.Web.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly IGenericRepository<Category> _categoryRepo;

        public CategoriesController(IGenericRepository<Category> categoryRepo)
        {
            _categoryRepo = categoryRepo;
        }

        // GET: Categories
        public async Task<IActionResult> Index()
        {
            var allCategories = await _categoryRepo.Get();

            var viewModel = Models.CategoryViewModel.categoryList(allCategories);

            return View(viewModel);
        }

        // GET: Categories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();

                var category = await _categoryRepo.GetById(okId);

                if (category == null)
                {
                    return NotFound();
                }

                var viewModel = Models.CategoryViewModel.categoryDetail(category);

                return View(viewModel);
            }
        }

        // GET: Categories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Active")] Models.CategoryViewModel category)
        {
            if (ModelState.IsValid)
            {
                var model = Models.CategoryViewModel.categoryBuild(category);

                await _categoryRepo.Insert(model);
                await _categoryRepo.Save();

                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();

                var category = await _categoryRepo.GetById(okId);

                if (category == null)
                {
                    return NotFound();
                }

                var viewModel = Models.CategoryViewModel.categoryDetail(category);

                return View(viewModel);
            }
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Active")] Models.CategoryViewModel category)
        {
            if (id != category.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var model = Models.CategoryViewModel.categoryBuild(category);

                    _categoryRepo.Update(model);
                    await _categoryRepo.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    bool exist = await CategoryExists(category.Id);

                    if (! exist)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();

                var category = await _categoryRepo.GetById(okId);

                if (category == null)
                {
                    return NotFound();
                }

                var viewModel = Models.CategoryViewModel.categoryDetail(category);

                return View(viewModel);
            }
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _categoryRepo.Delete(id);
            await _categoryRepo.Save();

            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> CategoryExists(int id)
        {
            return await _categoryRepo.Exists(id);
        }
    }
}
