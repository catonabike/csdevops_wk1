﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Web.Models;
using Producks.Web.Repository;

namespace Producks.Web.Controllers
{
    [ApiController]
    public class ExportsController : ControllerBase
    {
        private readonly IGenericRepository<Product> _productRepo;

        public ExportsController(IGenericRepository<Product> productRepo)
        {
            _productRepo = productRepo;
        }

        // GET: api/Brands
        [HttpGet("api/Brands")]
        public async Task<IActionResult> GetBrands()
        {
            var pList = await _productRepo.Get(
                includeProperties: "Category,Brand");

            var bl = ProductViewModel.showActiveBrand(pList);

            return Ok(bl);
        }

        // GET: api/Categories
        [HttpGet("api/Categories")]
        public async Task<IActionResult> GetCategories()
        {
            var pList = await _productRepo.Get(
                includeProperties: "Category,Brand");

            var cl = ProductViewModel.showActiveCategory(pList);

            return Ok(cl);
        }

        // GET: api/Categories?categoryId=3&brandId=4&minPrice=20&maxPrice=100
        [HttpGet("api/Products")]
        public async Task<IActionResult> GetProducts(int? categoryId, int? brandId, int? minPrice, int? maxPrice)
        {
            var pList = await _productRepo.Get(
                includeProperties: "Category,Brand");

            var pl = ProductViewModel.showActiveProduct(pList, categoryId, brandId, minPrice, maxPrice);

            return Ok(pl);
        }
    }
}
