using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Producks.Data;
using Producks.Web.Repository;

namespace Producks.Web.Controllers
{
    public class BrandsController : Controller
    {
        private IGenericRepository<Brand> _brandRepo;

        public BrandsController(IGenericRepository<Brand> brandRepo)
        {
            _brandRepo = brandRepo;
        }

        // GET: Brands
        public async Task<IActionResult> Index()
        {
            var allBrands = await _brandRepo.Get();

            var viewModel = Models.BrandViewModel.brandList(allBrands);

            return View(viewModel);
        }

        // GET: Brands/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();
                
                var brand = await _brandRepo.GetById(okId);

                if (brand == null)
                {
                    return NotFound();
                }

                var viewModel = Models.BrandViewModel.brandDetail(brand);

                return View(viewModel);
            }
        }

        // GET: Brands/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Brands/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Active")] Models.BrandViewModel brand)
        {
            if (ModelState.IsValid)
            {
                var model = Models.BrandViewModel.brandBuild(brand);

                await _brandRepo.Insert(model);
                await _brandRepo.Save();

                return RedirectToAction(nameof(Index));
            }
            return View(brand);
        }

        // GET: Brands/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();
                
                var brand = await _brandRepo.GetById(okId);

                if (brand == null)
                {
                    return NotFound();
                }

                var viewModel = Models.BrandViewModel.brandDetail(brand);

                return View(viewModel);
            }
        }

        // POST: Brands/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Active")] Models.BrandViewModel brand)
        {
            if (id != brand.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var model = Models.BrandViewModel.brandBuild(brand);
;
                    _brandRepo.Update(model);
                    await _brandRepo.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    bool exist = await BrandExists(brand.Id);

                    if (! exist)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(brand);
        }

        // GET: Brands/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int okId = id.GetValueOrDefault();
                
                var brand = await _brandRepo.GetById(okId);

                if (brand == null)
                {
                    return NotFound();
                }

                var viewModel = Models.BrandViewModel.brandDetail(brand);

                return View(viewModel);
            }
        }

        // POST: Brands/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _brandRepo.Delete(id);
            await _brandRepo.Save();

            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> BrandExists(int id)
        {
            return await _brandRepo.Exists(id);
        }
    }
}
