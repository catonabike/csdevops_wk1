﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Producks.Data;
using Producks.Web.Models;
using Producks.Web.External;
using Producks.Web.Repository;

namespace Producks.Web.Controllers
{
    public class StoreController : Controller
    {
        private readonly IGenericRepository<Product> _productRepo;
        private readonly Undercutters _undercutters;

        public StoreController(IGenericRepository<Product> productRepo)
        {
            _productRepo = productRepo;
            _undercutters = new Undercutters();
        }

        // GET: Store
        public async Task<IActionResult> Index()
        {
            Tools.UnderCategoresDtoCompare categoryComparer = new Tools.UnderCategoresDtoCompare();
            Tools.UnderBrandsDtoCompare brandComparer = new Tools.UnderBrandsDtoCompare();

            // Get Category data from undercutters.
            await _undercutters.getUcCategories();
            var ucCatList = _undercutters.UcCategories;
            // Get Brand data from undercutters.
            await _undercutters.getUcBrands();
            var ucBrList = _undercutters.UcBrands;

            var pList = await _productRepo.Get(
                    includeProperties: "Category,Brand");

            var bl = UnderBrandDTO.acticeBrands(pList);
            var cl = UnderCategoryDTO.activeCategories(pList);

            // Combine the local and undercutters lists (Uses a comparer so no duplicates! ).
            var fullCatList = cl.Union(ucCatList, categoryComparer);
            var fullBrList = bl.Union(ucBrList, brandComparer);

            // Convert to a List.
            List<SelectListItem> fullCatListX = new SelectList(fullCatList, "Name", "Name").ToList();
            List<SelectListItem> fullBrListX = new SelectList(fullBrList, "Name", "Name").ToList();

            // Add an "All" entry @ Id=0 for the local lists so we can select all entries for category or brand!
            fullCatListX.Insert(0, (new SelectListItem { Text="All", Value="All", Selected=true }));           
            fullBrListX.Insert(0, (new SelectListItem { Text="All", Value="All", Selected=true }));

            ViewData["CategoryId"] = fullCatListX;
            ViewData["BrandId"] = fullBrListX;
            return View();
        }

        // GET: Store/ShowProducts
        public async Task<IActionResult> ShowProducts(string categoryId, string brandId)
        {
            if (categoryId == null && brandId == null)
            {
                return NotFound();
            }

            List<Product> pList;
            Tools.UnderProductsDtoCompare productComparer = new Tools.UnderProductsDtoCompare();

            // List of categories from undercutters.
            await _undercutters.getUcProducts();
            var ucProdList = _undercutters.UcProducts;

            // List of products.
            pList = await _productRepo.Get(
                    includeProperties: "Category,Brand");
            
            // Get both lists in the same format.
            var loProdList = UnderProductDTO.toProductList(pList, categoryId, brandId);
            var fucProdList = UnderProductDTO.filterUcProducts(ucProdList, categoryId, brandId);

            // Combine the local and undercutters lists (Uses a comparer so no duplicates! ).
            var fullProdList = loProdList.Union(fucProdList, productComparer);

            return View(fullProdList);
        }
    }
}